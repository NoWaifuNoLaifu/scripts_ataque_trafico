import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pickle
import joblib

bankdata = pd.read_csv("f_1702d.csv")
print bankdata.shape
print bankdata.head()


X = bankdata.drop('Class', axis=1)
y = bankdata['Class']


from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.30)

print X_test.head()
from sklearn.svm import SVC
"""
svclassifier = SVC(kernel='linear')
svclassifier.fit(X_train, y_train)
#fin model 4
"""
"""
svclassifier = SVC(kernel='poly', degree=100, C=0.001,gamma=0.001)
svclassifier.fit(X_train, y_train)
#fin model 3
"""

"""
svclassifier = SVC(kernel='rbf')
svclassifier.fit(X_train, y_train)"""
#finalized_model1.sav
from sklearn.model_selection import GridSearchCV
from sklearn import svm

"""
def svc_param_selection(X, y, nfolds):
    Cs = [0.001]
    gammas = [0.001, 0.01]
    param_grid = {'C': Cs, 'gamma' : gammas}
    grid_search = GridSearchCV(svm.SVC(kernel='rbf', degree = 100), param_grid, cv=nfolds)
    grid_search.fit(X, y)
    grid_search.best_params_
    return grid_search.best_params_

print (svc_param_selection(X,y,10))
"""


# Import the model we are using
from sklearn.ensemble import RandomForestRegressor# Instantiate model with 1000 decision trees
rf = RandomForestRegressor(n_estimators = 1000, random_state = 42)# Train the model on training data
rf.fit(X_train, y_train);



"""
#fin model 2
svclassifier = SVC(kernel='sigmoid')
svclassifier.fit(X_train, y_train)
"""

y_pred = rf.predict(X_test)

from sklearn.metrics import classification_report, confusion_matrix
print(confusion_matrix(y_test,y_pred))
print(classification_report(y_test,y_pred))

filename = 'finalized_model_rf_vanilla.sav'
joblib.dump(rf, filename)
