import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pickle
import joblib

bankdata = pd.read_csv("f_1702d.csv")
print bankdata.shape
print bankdata.head()


X = bankdata.drop('Class', axis=1)
y = bankdata['Class']


from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.30)

print X_test.head()
#linear train
from sklearn.svm import SVC
"""
svclassifier = SVC(kernel='linear')
svclassifier.fit(X_train, y_train)
#fin model 4
"""
#poly train
"""
svclassifier = SVC(kernel='poly', degree=100, C=0.001,gamma=0.001)
svclassifier.fit(X_train, y_train)
#fin model 3
"""
#rbf train
"""
svclassifier = SVC(kernel='rbf')
svclassifier.fit(X_train, y_train)

"""
#finalized_model1.sav
from sklearn.model_selection import GridSearchCV
from sklearn import svm

##optimization grid search
"""

def svc_param_selection(X, y, nfolds):
    Cs = [0.001, 0.01, 0.1, 1, 10]
    gammas = [0.001, 0.01, 0.1, 1]
    param_grid = {'C': Cs, 'gamma' : gammas}
    grid_search = GridSearchCV(svm.SVC(kernel='sigmoid'), param_grid, cv=nfolds)
    grid_search.fit(X, y)
    grid_search.best_params_
    return grid_search.best_params_

print (svc_param_selection(X,y,10))

"""
## sigmoid train
"""
#fin model 2
svclassifier = SVC(kernel='sigmoid')
svclassifier.fit(X_train, y_train)
"""

#modelo lineal de SVM
filename1 = '/root/Desktop/poxu/ext/finalized_model4.sav'
model1 = joblib.load(filename1)
#modelo poly optimizado
filename2 = '/root/Desktop/poxu/ext/finalized_modelpoly(candgamma 001).sav'
model2 = joblib.load(filename2)
#modelo rbf
filename3 = '/root/Desktop/poxu/ext/finalized_model1.sav'
model3 = joblib.load(filename3)

#modelo sigmoid
filename4 = '/root/Desktop/poxu/ext/finalized_model2.sav'
model4 = joblib.load(filename4)
print("modelo 1\n")
#estadisticas modelo1
y_pred = model1.predict(X_test)

from sklearn.metrics import classification_report, confusion_matrix
print(confusion_matrix(y_test,y_pred))
print(classification_report(y_test,y_pred))
print("modelo 2\n")
#estadisticas modelo2
y_pred = model2.predict(X_test)

from sklearn.metrics import classification_report, confusion_matrix
print(confusion_matrix(y_test,y_pred))
print(classification_report(y_test,y_pred))
print("modelo 3\n")
#estadisticas modelo3
y_pred = model3.predict(X_test)

from sklearn.metrics import classification_report, confusion_matrix
print(confusion_matrix(y_test,y_pred))
print(classification_report(y_test,y_pred))
print("modelo 4\n")
#estadisticas modelo4
y_pred = model4.predict(X_test)

from sklearn.metrics import classification_report, confusion_matrix
print(confusion_matrix(y_test,y_pred))
print(classification_report(y_test,y_pred))




#save mechanism
#filename = 'finalized_modelpoly(candgamma 001).sav'
#joblib.dump(svclassifier, filename)

