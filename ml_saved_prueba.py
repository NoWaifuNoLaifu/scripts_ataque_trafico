from __future__ import absolute_import, division, print_function, unicode_literals
import os 
#import matplotlib.pyplot as plt
import tensorflow as tf

print(tf.__version__)
column_names=['lenght','table_id', 'in_port','dl_vlan','dl_type','nw_tos','nw_proto', 'tp_src','tp_dst','duration_sec','duration_nsec','priority','idle_timeout','hard_timeout','cookie','packet_count','byte_count','flow_status']
feature_names = column_names[:-1]
label_name = column_names[-1]

print("Features:{}".format(feature_names))
print("label:{}".format(label_name))
class_names = ['attack', 'normal']

batch_size = 32
new_model = tf.keras.models.load_model('model_prueba.h5')

# Show the model architecture
new_model.summary()

def pack_features_vector(features, labels):
	"""pack features into a 1D array"""
	features = tf.stack(list(features.values()),axis=1)
	return features, labels


loss_object = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)

def loss(model, x, y):
	y_ = model(x)
	return loss_object(y_true= y,y_pred=y_)

l = loss(model, features, labels)
print("Loss test: {}".format(l))

def grad(model, inputs, targets):
	with tf.GradientTape() as tape:
		loss_value = loss(model, inputs, targets)
	return loss_value, tape.gradient(loss_value, model.trainable_variables)


test_fp = "f_nos.csv"
test_dataset = tf.data.experimental.make_csv_dataset(
	test_fp,
	batch_size,
	column_names=column_names,
	label_name='flow_status',
	num_epochs=1,
	shuffle=False)

test_dataset = test_dataset.map(pack_features_vector)
test_accuracy = tf.keras.metrics.Accuracy()

for (x, y) in test_dataset:
	logits = model(x)
	prediction = tf.argmax(logits, axis=1, output_type=tf.int32)
	test_accuracy(prediction, y)
print ("Test set accuracy: {:.3%}".format(test_accuracy.result()))
tf.stack([y, prediction], axis=1)

