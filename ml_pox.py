from __future__ import absolute_import, division, print_function, unicode_literals
import os 
#import matplotlib.pyplot as plt
import tensorflow as tf

print(tf.__version__)

#
train_dataset_file = "dataset_limpio_nos_mejorado.csv"
column_names=['length','in_port','nw_proto','tp_src','tp_dst','duration_sec','duration_nsec','priority','hard_timeout','packet_count','byte_count','flow_status']
feature_names = column_names[:-1]
label_name = column_names[-1]

print("Features:{}".format(feature_names))
print("label:{}".format(label_name))
class_names = ['attack', 'normal']

batch_size = 32
train_dataset = tf.data.experimental.make_csv_dataset(
	train_dataset_file,
	batch_size,
	column_names = column_names,
	label_name = label_name,
	num_epochs = 1)

features, labels = next(iter(train_dataset))
print(features)

def pack_features_vector(features, labels):
	"""pack features into a 1D array"""
	features = tf.stack(list(features.values()),axis=1)
	return features, labels

train_dataset = train_dataset.map(pack_features_vector)
features,labels = next(iter(train_dataset))
print(features[:5])

model = tf.keras.Sequential([
 tf.keras.layers.Dense(10, activation=tf.nn.relu,input_shape=(11,)),
 tf.keras.layers.Dense(10,activation=tf.nn.relu),
 tf.keras.layers.Dense(2)
])

loss_object = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)

def loss(model, x, y, training):
	y_ = model(x, training=training)
	return loss_object(y_true=y,y_pred=y_)

l = loss(model, features, labels, training=False)


def grad(model, inputs, targets):
	with tf.GradientTape() as tape:
		loss_value = loss(model, inputs, targets, training=False)
	return loss_value, tape.gradient(loss_value, model.trainable_variables)

optimizer = tf.keras.optimizers.SGD(learning_rate=0.01)
loss_value, grads = grad(model, features, labels)
optimizer.apply_gradients(zip(grads, model.trainable_variables))

train_loss_results = []
train_accuracy_results = []

num_epochs = 50

for epoch in range(num_epochs):
	epoch_loss_avg = tf.keras.metrics.Mean()
	epoch_accuracy = tf.keras.metrics.SparseCategoricalAccuracy()
	
	for x, y in train_dataset:
		loss_value, grads = grad(model, x, y)
		optimizer.apply_gradients(zip(grads,model.trainable_variables))
		epoch_loss_avg(loss_value)
		epoch_accuracy(y, model(x))
	train_loss_results.append(epoch_loss_avg.result())
	train_accuracy_results.append(epoch_accuracy.result())
	if epoch % 50 == 0:
		print ("epoch {:03d} : loss : {:.3f}, Accuracy {:.3%}".format(epoch, epoch_loss_avg.result(), epoch_accuracy.result()))
 
test_fp = "f_nos_mejorado.csv"
test_dataset = tf.data.experimental.make_csv_dataset(
	test_fp,
	batch_size,
	column_names=column_names,
	label_name='flow_status',
	num_epochs=1,
	shuffle=False)

test_dataset = test_dataset.map(pack_features_vector)
test_accuracy = tf.keras.metrics.Accuracy()

for (x, y) in test_dataset:
	logits = model(x)
	prediction = tf.argmax(logits, axis=1, output_type=tf.int32)
	test_accuracy(prediction, y)
print ("Test set accuracy: {:.3%}".format(test_accuracy.result()))
tf.stack([y, prediction], axis=1)

model.save('model_first_dataset.h5')
